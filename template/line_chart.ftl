<div class="widget-box transparent">
                              <div class="widget-header widget-header-flat">
                                 <h4 class="widget-title lighter">
                                    <i class="ace-icon fa fa-signal"></i>
                                    Sale Stats
                                 </h4>
                                 <div class="widget-toolbar">
                                    <a href="#" data-action="collapse">
                                    <i class="ace-icon fa fa-chevron-up"></i>
                                    </a>
                                 </div>
                              </div>
                              <div class="widget-body">
                                 <div class="widget-main padding-4">
                                    <div id="sales-charts"></div>
                                 </div>
                                 <!-- /.widget-main -->
                              </div>
                              <!-- /.widget-body -->
                           </div>
                           <!-- /.widget-box -->
						   
						   
						   <script type="text/javascript">
						   jQuery(function($) {
						   var d1 = [];
         	for (var i = 0; i < Math.PI * 2; i += 0.5) {
         		d1.push([i, Math.sin(i)]);
         	}
         
         	var d2 = [];
         	for (var i = 0; i < Math.PI * 2; i += 0.5) {
         		d2.push([i, Math.cos(i)]);
         	}
         
         	var d3 = [];
         	for (var i = 0; i < Math.PI * 2; i += 0.2) {
         		d3.push([i, Math.tan(i)]);
         	}
         	
         
         	var sales_charts = $('#sales-charts').css({'width':'100%' , 'height':'220px'});
         	$.plot("#sales-charts", [
         		{ label: "Domains", data: d1 },
         		{ label: "Hosting", data: d2 },
         		{ label: "Services", data: d3 }
         	], {
         		hoverable: true,
         		shadowSize: 0,
         		series: {
         			lines: { show: true },
         			points: { show: true }
         		},
         		xaxis: {
         			tickLength: 0
         		},
         		yaxis: {
         			ticks: 10,
         			min: -2,
         			max: 2,
         			tickDecimals: 3
         		},
         		grid: {
         			backgroundColor: { colors: [ "#fff", "#fff" ] },
         			borderWidth: 1,
         			borderColor:'#555'
         		}
         	});
         })
						   </script>