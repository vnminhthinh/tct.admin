package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.MenuTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MenuTransRepo extends JpaRepository<MenuTranslation, MenuTranslation.MenuTranslationPK> {
    public MenuTranslation findByMenuTranslationPK(MenuTranslation.MenuTranslationPK menuTranslationPK);
    public MenuTranslation findByMenuIdAndLanguageCode(Long menuId, String code);
}
