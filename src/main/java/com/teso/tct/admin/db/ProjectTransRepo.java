package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.ProjectTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProjectTransRepo extends JpaRepository<ProjectTranslation,ProjectTranslation.ProjectTranslationPK> {
    public ProjectTranslation findByProjectIdAndLanguageCode(Long projectNonStranId, String code);
}
