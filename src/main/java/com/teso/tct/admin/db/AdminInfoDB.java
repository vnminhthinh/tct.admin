package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.AdminInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


/**
 * AdminInfo Repositories
 *
 * @author Anh TST
 */
@Repository
public interface AdminInfoDB extends JpaRepository<AdminInfo, Integer> {

  AdminInfo findByEmail(@Param("email") String email);
}
