package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.PermissionInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


/**
 * PermissionInfo Repositories
 *
 * @author Anh TST
 */
@Repository
public interface PermissionInfoDB extends JpaRepository<PermissionInfo, Integer> {

}
