package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Posts;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PostsDB extends JpaRepository<Posts, String> {
}
