package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepo extends JpaRepository<Language,Long> {
}
