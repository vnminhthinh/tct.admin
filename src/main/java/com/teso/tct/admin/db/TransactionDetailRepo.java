package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.TransactionDetail;
import com.teso.tct.admin.ent.TransactionDetail.TransactionDetailPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface TransactionDetailRepo extends JpaRepository<TransactionDetail, TransactionDetailPK> {
    public List<TransactionDetail> findByTransactionId(String id);
    public List<TransactionDetail> findByItemId(Long itemId);

}
