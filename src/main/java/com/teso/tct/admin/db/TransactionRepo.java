package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionRepo extends JpaRepository<Transaction, Long> {
    public Transaction findItemById(@Param("id") String id);
}
