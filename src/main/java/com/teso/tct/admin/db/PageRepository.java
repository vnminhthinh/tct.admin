package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PageRepository extends JpaRepository<Item, Integer>{
    Page<Item> findAll(Pageable pageable);
}
