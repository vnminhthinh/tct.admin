package com.teso.tct.admin.db;

import com.teso.tct.admin.ent.ItemTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemTransRepo extends JpaRepository<ItemTranslation, ItemTranslation.ItemTranslationPK> {
    public ItemTranslation findByItemIdAndLanguageCode(Long itemId, String code);
}

