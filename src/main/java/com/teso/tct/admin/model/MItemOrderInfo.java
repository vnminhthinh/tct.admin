package com.teso.tct.admin.model;

/**
 * @author Anh TST
 */
public class MItemOrderInfo {

  private int id;
  private String refId;
  private String name;
  private String image;
  private int price;
  private int quantity;
  private String provider;
  private String status;
  private String contact;
  private String dateCreate;
  private String dateReceived;

  public MItemOrderInfo() {
  }

  public MItemOrderInfo(int id, String refId, String name, String image, int price, int quantity,
      String provider, String status, String contact, String dateCreate,
      String dateReceived) {
    this.id = id;
    this.refId = refId;
    this.name = name;
    this.image = image;
    this.price = price;
    this.quantity = quantity;
    this.provider = provider;
    this.status = status;
    this.contact = contact;
    this.dateCreate = dateCreate;
    this.dateReceived = dateReceived;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getRefId() {
    return refId;
  }

  public void setRefId(String refId) {
    this.refId = refId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public int getPrice() {
    return price;
  }

  public void setPrice(int price) {
    this.price = price;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public String getProvider() {
    return provider;
  }

  public void setProvider(String provider) {
    this.provider = provider;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }

  public String getDateCreate() {
    return dateCreate;
  }

  public void setDateCreate(String dateCreate) {
    this.dateCreate = dateCreate;
  }

  public String getDateReceived() {
    return dateReceived;
  }

  public void setDateReceived(String dateReceived) {
    this.dateReceived = dateReceived;
  }
}
