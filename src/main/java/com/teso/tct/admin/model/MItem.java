package com.teso.tct.admin.model;

import java.util.Date;

public class MItem {
    private Long id;
    private String imgUrl;
    private String title;
    private String description;
    private String content;
    private double price;
    private double promotion;
    private Date createdTime;
    private MProject mProject;

    public MItem() {
    }

    public MItem(Long id, String imgUrl, String title, String description, String content, double price, double promotion, Date createdTime, MProject mProject) {
        this.id = id;
        this.imgUrl = imgUrl;
        this.title = title;
        this.description = description;
        this.content = content;
        this.price = price;
        this.promotion = promotion;
        this.createdTime = createdTime;
        this.mProject = mProject;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getPromotion() {
        return promotion;
    }

    public void setPromotion(double promotion) {
        this.promotion = promotion;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public MProject getmProject() {
        return mProject;
    }

    public void setmProject(MProject mProject) {
        this.mProject = mProject;
    }
}
