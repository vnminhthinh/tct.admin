package com.teso.tct.admin.model;

/**
 * @author Anh TST
 */
public class MCustomerOrderInfo {

  private int id;
  private String refId;
  private String csName;
  private String state;
  private String dateCreate;
  private String dateTried;
  private String dateSend;
  private long prepaid;
  private long liabilities;
  private long total;

  public MCustomerOrderInfo() {
  }

  public MCustomerOrderInfo(int id, String refId, String csName, String state,
      String dateCreate, String dateTried, String dateSend, long prepaid, long liabilities,
      long total) {
    this.id = id;
    this.refId = refId;
    this.csName = csName;
    this.state = state;
    this.dateCreate = dateCreate;
    this.dateTried = dateTried;
    this.dateSend = dateSend;
    this.prepaid = prepaid;
    this.liabilities = liabilities;
    this.total = total;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getRefId() {
    return refId;
  }

  public void setRefId(String refId) {
    this.refId = refId;
  }

  public String getCsName() {
    return csName;
  }

  public void setCsName(String csName) {
    this.csName = csName;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getDateCreate() {
    return dateCreate;
  }

  public void setDateCreate(String dateCreate) {
    this.dateCreate = dateCreate;
  }

  public String getDateTried() {
    return dateTried;
  }

  public void setDateTried(String dateTried) {
    this.dateTried = dateTried;
  }

  public String getDateSend() {
    return dateSend;
  }

  public void setDateSend(String dateSend) {
    this.dateSend = dateSend;
  }

  public long getPrepaid() {
    return prepaid;
  }

  public void setPrepaid(long prepaid) {
    this.prepaid = prepaid;
  }

  public long getLiabilities() {
    return liabilities;
  }

  public void setLiabilities(long liabilities) {
    this.liabilities = liabilities;
  }

  public long getTotal() {
    return total;
  }

  public void setTotal(long total) {
    this.total = total;
  }
}
