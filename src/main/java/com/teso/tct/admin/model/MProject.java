package com.teso.tct.admin.model;

import com.teso.tct.admin.ent.Menu;

import java.util.Date;
import java.util.List;

public class MProject {
    private Long id;
    private String name;
    private String description;
    private String content;
    private String thumbnail;
    private String type;
    private Date createdTime;
    private MMenu mMenu;
    private List<MItem> items;

    public MProject() {
    }

    public MProject(Long id, String name, String description, String content, String thumbnail, String type, Date createdTime, MMenu mMenu, List<MItem> items) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.content = content;
        this.thumbnail = thumbnail;
        this.type = type;
        this.createdTime = createdTime;
        this.mMenu = mMenu;
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public List<MItem> getItems() {
        return items;
    }

    public void setItems(List<MItem> items) {
        this.items = items;
    }

    public MMenu getmMenu() {
        return mMenu;
    }

    public void setmMenu(MMenu mMenu) {
        this.mMenu = mMenu;
    }
}
