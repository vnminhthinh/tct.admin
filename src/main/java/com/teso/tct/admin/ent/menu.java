package com.teso.tct.admin.ent;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;



@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String thumbnail;
    private String type;
    private Date createdTime;


    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "menu")
    private Set<Project> projects = new HashSet<>();

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "menu")
    @JsonIgnoreProperties("menu")
    private Set<MenuTranslation> menuTranslations = new HashSet<>();

    public Menu() {
    }

    public Menu(Long id, String thumbnail, String type, Date createdTime) {
        this.id = id;
        this.thumbnail = thumbnail;
        this.type = type;
        this.createdTime = createdTime;
    }

    public Date getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }

    public Set<MenuTranslation> getMenuTranslations() {
        return menuTranslations;
    }

    public void setMenuTranslations(Set<MenuTranslation> menuTranslations) {
        this.menuTranslations = menuTranslations;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
    @PrePersist
    void onCreate() {
        this.setCreatedTime(new Date(System.currentTimeMillis()));
    }

}
