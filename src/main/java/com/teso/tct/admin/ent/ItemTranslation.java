package com.teso.tct.admin.ent;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "item_translation")
public class ItemTranslation {

    @Embeddable
    public static class ItemTranslationPK implements Serializable {

        private Long itemNonStranId;
        private Long languageId;

        public ItemTranslationPK() {
        }

        public ItemTranslationPK(Long itemNonStranId, Long languageId) {
            this.itemNonStranId = itemNonStranId;
            this.languageId = languageId;
        }

        public Long getItemNonStranId() {
            return itemNonStranId;
        }

        public void setItemNonStranId(Long itemNonStranId) {
            this.itemNonStranId = itemNonStranId;
        }

        public Long getLanguageId() {
            return languageId;
        }

        public void setLanguageId(Long languageId) {
            this.languageId = languageId;
        }

    }
    @EmbeddedId
    private ItemTranslationPK itemTranslationPK;

    @MapsId("ItemNonStranId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "itemNonStranId", referencedColumnName = "id")
    private Item item;

    @MapsId("LaguageId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "languageId", referencedColumnName = "id")
    private Language language;

    private String title;
    @Lob
    private String description;
    @Lob
    private String content;


    public ItemTranslation() {
    }

    public ItemTranslation(ItemTranslationPK itemTranslationPK, Item item, Language language, String title, String description, String content) {
        this.itemTranslationPK = itemTranslationPK;
        this.item = item;
        this.language = language;
        this.title = title;
        this.description = description;
        this.content = content;
    }

    public ItemTranslationPK getItemTranslationPK() {
        return itemTranslationPK;
    }

    public void setItemTranslationPK(ItemTranslationPK itemTranslationPK) {
        this.itemTranslationPK = itemTranslationPK;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
