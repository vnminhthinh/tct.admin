package com.teso.tct.admin.ent;

import java.io.Serializable;
import javax.persistence.*;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "project_translation")
public class ProjectTranslation {

    @Embeddable
    public static class ProjectTranslationPK implements Serializable {

        private Long projectNonStranId;
        private Long languageId;

        public ProjectTranslationPK() {
        }

        public ProjectTranslationPK(Long projectNonStranId, Long languageId) {
            this.projectNonStranId = projectNonStranId;
            this.languageId = languageId;
        }

        public Long getProjectNonStranId() {
            return projectNonStranId;
        }

        public void setProjectNonStranId(Long projectNonStranId) {
            this.projectNonStranId = projectNonStranId;
        }

        public Long getLanguageId() {
            return languageId;
        }

        public void setLanguageId(Long languageId) {
            this.languageId = languageId;
        }

    }
    @EmbeddedId
    private ProjectTranslationPK projectTranslationPK;

    @MapsId("ProjectNonStranId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "projectNonStranId", referencedColumnName = "id")
    private Project project;

    @MapsId("LaguageId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "languageId", referencedColumnName = "id")
    private Language language;

    private String name;
    private String description;
    @Lob
    private String content;

    public ProjectTranslation() {
    }

    public ProjectTranslation(ProjectTranslationPK projectTranslationPK, Project project, Language language, String name, String description, String content) {
        this.projectTranslationPK = projectTranslationPK;
        this.project = project;
        this.language = language;
        this.name = name;
        this.description = description;
        this.content = content;
    }

    public ProjectTranslationPK getProjectTranslationPK() {
        return projectTranslationPK;
    }

    public void setProjectTranslationPK(ProjectTranslationPK projectTranslationPK) {
        this.projectTranslationPK = projectTranslationPK;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
