package com.teso.tct.admin.ent;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "menu_translation")
public class MenuTranslation {

    @Embeddable
    public static class MenuTranslationPK implements Serializable {

        private Long menuNonStranId;
        private Long languageId;

        public MenuTranslationPK() {
        }

        public MenuTranslationPK(Long menuNonStranId, Long languageId) {
            this.menuNonStranId = menuNonStranId;
            this.languageId = languageId;
        }

        public Long getMenuNonStranId() {
            return menuNonStranId;
        }

        public void setMenuNonStranId(Long menuNonStranId) {
            this.menuNonStranId = menuNonStranId;
        }

        public Long getLanguageId() {
            return languageId;
        }

        public void setLanguageId(Long languageId) {
            this.languageId = languageId;
        }

    }
    @EmbeddedId
    private MenuTranslationPK menuTranslationPK;

    @MapsId("MenuNonStranId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "menuNonStranId", referencedColumnName = "id")
    private Menu menu;

    @MapsId("LaguageId")
    @ManyToOne(optional = false)
    @JoinColumn(name = "languageId", referencedColumnName = "id")
    private Language language;

    private String name;
    private String description;
    private String content;


    public MenuTranslation() {
    }

    public MenuTranslation(MenuTranslationPK menuTranslationPK, Menu menu, Language language, String name, String description, String content) {
        this.menuTranslationPK = menuTranslationPK;
        this.menu = menu;
        this.language = language;
        this.name = name;
        this.description = description;
        this.content = content;
    }

    public MenuTranslationPK getMenuTranslationPK() {
        return menuTranslationPK;
    }

    public void setMenuTranslationPK(MenuTranslationPK menuTranslationPK) {
        this.menuTranslationPK = menuTranslationPK;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
