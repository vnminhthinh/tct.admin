package com.teso.tct.admin.ent;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Table(name = "project")
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String thumbnail;
    private String type;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "project")
    private Set<Item> items = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "menu_id", nullable = true)
    @JsonIgnoreProperties("projects")
    private Menu menu;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdTime;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "project")
    @JsonIgnoreProperties("project")
    private Set<ProjectTranslation> projectTranslations = new HashSet<>();

    public Project() {

    }

    public Project(Long id, String thumbnail, String type, Menu menu, Date createdTime) {
        this.id = id;
        this.thumbnail = thumbnail;
        this.type = type;
        this.menu = menu;
        this.createdTime = createdTime;
    }

    public Set<ProjectTranslation> getProjectTranslations() {
        return projectTranslations;
    }

    public void setProjectTranslations(Set<ProjectTranslation> projectTranslations) {
        this.projectTranslations = projectTranslations;
    }

    public Date getCreatedTime() {
        return createdTime;
    }


    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Set<Item> getItems() {
        return items;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setItems(Set<Item> items) {
        this.items = items;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public void setCreatedTime(Date createdTime) {
        this.createdTime = createdTime;
    }
    @PrePersist
    void onCreate() {
        this.setCreatedTime(new Date(System.currentTimeMillis()));
    }
}

