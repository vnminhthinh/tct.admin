package com.teso.tct.admin.api;

import com.teso.tct.admin.db.AdminInfoDB;
import com.teso.tct.admin.db.PermissionInfoDB;
import com.teso.tct.admin.ent.AdminInfo;
import com.teso.tct.admin.ent.PermissionInfo;
import com.teso.tct.admin.model.MAdminInfo;
import com.teso.tct.admin.model.MPermissionInfo;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import com.teso.framework.utils.ConvertUtils;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminController {

    @Autowired
    private AdminInfoDB adminInfoDB;

    @Autowired
    private PermissionInfoDB permissionInfoDB;

    @RequestMapping("/adminInfo")
    public String information(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("active_admin_info", true);

        List<MAdminInfo> mAdminInfoList = adminInfoDB.findAll().stream().map(p
                -> new MAdminInfo(p.getId(), p.getName(), p.getEmail(), p.getRole(), p.getPermissionInfo().getName()))
                .collect(Collectors.toList());

        model.addAttribute("page_content", "admin.ftl");
        model.addAttribute("mAdminInfoList", mAdminInfoList);

        List<MPermissionInfo> mPermissionInfoList = permissionInfoDB.findAll().stream().map(p -> {
            boolean canRemove = CollectionUtils.isEmpty(p.getAdminInfoList());
            return new MPermissionInfo(p.getId(), p.getName(), p.getRule(), canRemove);
        }).collect(Collectors.toList());

        model.addAttribute("mPermissionInfoList", mPermissionInfoList);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/adminRemoveUser")
    public void removeUser(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String id = request.getParameter("id");
        adminInfoDB.deleteById(ConvertUtils.toInt(id));
        response.sendRedirect("/adminInfo");
    }

    @RequestMapping("/adminPermission")
    public String permission(Model model, HttpServletRequest request,
                             HttpServletResponse response) {
        model.addAttribute("active_admin_perm", true);
        model.addAttribute("page_content", "admin.ftl");

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/adminSavePermission")
    public void savePermission(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String name = request.getParameter("permissionName");
        String rule;
        String dashboardState = "1";
        String adminState = "0";
        if (request.getParameter("adminState") != null) {
            adminState = request.getParameter("adminState").equals("on") ? "1" : "0";
        }

        String itemState = "0";
        if (request.getParameter("itemState") != null) {
            itemState = request.getParameter("itemState").equals("on") ? "1" : "0";
        }

        String orderState = "0";
        if (request.getParameter("orderState") != null) {
            orderState = request.getParameter("orderState").equals("on") ? "1" : "0";
        }
        rule = dashboardState + adminState + itemState + orderState;

        PermissionInfo permissionInfo = new PermissionInfo(name, rule);
        permissionInfoDB.save(permissionInfo);

        response.sendRedirect("/adminPermission");
    }

    @RequestMapping("/adminRemovePermission")
    public void removePermission(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String id = request.getParameter("id");
        permissionInfoDB.deleteById(ConvertUtils.toInt(id));
        response.sendRedirect("/adminInfo");
    }

    @RequestMapping("/adminUser")
    public String user(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("active_admin_user", true);

        model.addAttribute("page_content", "admin.ftl");

        List<MPermissionInfo> mPermissionInfoList = permissionInfoDB.findAll().stream().map(p -> {
            boolean canRemove = CollectionUtils.isEmpty(p.getAdminInfoList());
            return new MPermissionInfo(p.getId(), p.getName(), p.getRule(), canRemove);
        }).collect(Collectors.toList());

        model.addAttribute("mPermissionInfoList", mPermissionInfoList);

        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/adminAddUser")
    public void addUser(HttpServletRequest request, HttpServletResponse response) throws IOException {

        System.out.println(request.getParameter("permission"));
        String email = request.getParameter("email");
        String role = request.getParameter("role");
        String permission = request.getParameter("permission");

        PermissionInfo permissionInfo = permissionInfoDB.getOne(ConvertUtils.toInt(permission));
        AdminInfo adminInfo = new AdminInfo(email, role, permissionInfo);
        adminInfoDB.saveAndFlush(adminInfo);
        response.sendRedirect("/adminUser");
    }
}
