package com.teso.tct.admin.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.teso.tct.admin.config.ConfigInfo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {

    @RequestMapping(value = "/login")
    public String login(Model model, HttpServletRequest request, HttpServletResponse response) {
        System.err.println("-------------> LOGIN");
        model.addAttribute("company_name", ConfigInfo.COMPANY_NAME);
        model.addAttribute("client_id",ConfigInfo.CLIENT_ID);

        return "login";
    }
    
    @RequestMapping("/logout")
    public String logout(Model model, HttpServletRequest request, HttpServletResponse response) {
        System.err.println("-------------> LOGOUT");
        model.addAttribute("company_name", ConfigInfo.COMPANY_NAME);
        model.addAttribute("client_id",ConfigInfo.CLIENT_ID);
        return "login";
    }
}
