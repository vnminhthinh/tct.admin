package com.teso.tct.admin.api;

import com.teso.framework.utils.ConvertUtils;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.*;
import com.teso.tct.admin.ent.*;
import com.teso.tct.admin.model.*;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static com.teso.tct.admin.config.EntitiyExtension.updateItemToModel;
import static com.teso.tct.admin.config.EntitiyExtension.updateMenuToModel;
import static com.teso.tct.admin.config.EntitiyExtension.updateProjectToModel;

@Controller
public class ItemController {

    @Autowired
    private ProjectDB projectDB;

    @Autowired
    private ItemDB itemDB;
    @Autowired
    private ItemTransRepo itemTransRepo;

    @Autowired
    PageService pageService;

    @Autowired
    PageServiceProject pageServiceProject;
    @Autowired
    private ProjectTransRepo projectTransRepo;
    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private String IMAGE_DEFAULT = ConfigInfo.IMAGE_DEFAULT;
    private int PAGE_SIZE = ConvertUtils.toInt(ConfigInfo.PAGE_SIZE);
    private boolean PRICE = ConvertUtils.toBoolean(ConfigInfo.PRICE);
    private boolean menu = ConvertUtils.toBoolean(ConfigInfo.MENU);
    private boolean title = ConvertUtils.toBoolean(ConfigInfo.TITLE);
    private boolean content = ConvertUtils.toBoolean(ConfigInfo.CONTENT_ITEM);
    private boolean description = ConvertUtils.toBoolean(ConfigInfo.DESCRIPTION_ITEM);
    private List<MProject> projectModels;
    private List<MItem> mItems;
    private List<MItem> mItemsEn;

    @Autowired
    public ItemController(StorageService storageService) {
        this.storageService = storageService;
    }


    @RequestMapping("/items")
    public String orderItems(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "items.ftl");
        List<Project> projectList = projectDB.findAll();
        model.addAttribute("projectList", projectList);
        List<Item> items = itemDB.findAll();
        int totalPage = items.size();
        int j = totalPage % PAGE_SIZE;
        int k = totalPage / PAGE_SIZE;
        if (j > 0) {
            k++;
        }
        mItems = new ArrayList<>();
        for (int i = (page - 1) * PAGE_SIZE; i < page * PAGE_SIZE && i < items.size(); i++) {
            ItemTranslation itemTranslation = itemTransRepo.findByItemIdAndLanguageCode(items.get(i).getId(), "vi");
            Project project = items.get(i).getProject();
            ProjectTranslation projectTranslation = projectTransRepo.findById(new ProjectTranslation.ProjectTranslationPK(project.getId(), 1L)).get();
            MItem itemModel = updateItemToModel(items.get(i), itemTranslation, project, projectTranslation);
            mItems.add(itemModel);
        }
        mItemsEn = new ArrayList<>();
        for (Item item : items) {
            Project project = item.getProject();
            ItemTranslation itemTranslationEn = itemTransRepo.findByItemIdAndLanguageCode(item.getId(), "en");
            if (itemTranslationEn != null) {
                Project projectEn = item.getProject();
                ProjectTranslation projectTranslationEn = projectTransRepo.findById(new ProjectTranslation.ProjectTranslationPK(project.getId(), 1L)).get();
                MItem itemModelEn = updateItemToModel(item, itemTranslationEn, projectEn, projectTranslationEn);
                mItemsEn.add(itemModelEn);
            }
        }
        model.addAttribute("menu", menu);
        model.addAttribute("title", title);
        model.addAttribute("description", description);
        model.addAttribute("content", content);
        model.addAttribute("price", PRICE);
        model.addAttribute("itemList", mItems);
        model.addAttribute("upload_url", UPLOAD_URL);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_item")
    public String addItem(Model model, HttpServletRequest request,
                          HttpServletResponse response) {
        model.addAttribute("page_content", "add_items.ftl");
        List<Project> projects = projectDB.findAll();
        projectModels = new ArrayList();
        for (Project project : projects) {
            ProjectTranslation projectTranslation = projectTransRepo.findByProjectIdAndLanguageCode(project.getId(), "vi");
            MProject projectModel = updateProjectToModel(project, projectTranslation);
            projectModels.add(projectModel);
        }
        model.addAttribute("projects", projectModels);
        model.addAttribute("price", PRICE);
        model.addAttribute("menu", menu);
        model.addAttribute("title", title);
        model.addAttribute("description", description);
        model.addAttribute("content", content);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }


    @PostMapping("/createItem")
    public void createItemOrder(@RequestParam("imgUrl") MultipartFile files, HttpServletRequest request,
                                HttpServletResponse response) throws IOException, ParseException {
        String content = request.getParameter("content");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String price = request.getParameter("price");
        String project_id = request.getParameter("project_id");
        String language_id = request.getParameter("language_id");
        Project project = projectDB.findById(ConvertUtils.toLong(project_id)).get();
        Item item = new Item();
        item.setProject(project);
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            item.setImgUrl(UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            item.setImgUrl(UPLOAD_FOLDER + IMAGE_DEFAULT);
        }
        item.setProject(project);
        if (PRICE == true) {
            item.setPrice(ConvertUtils.toDouble(price));
        }
        Long id = itemDB.save(item).getId();
        if(ConvertUtils.toLong(language_id)==2){
            ItemTranslation itemTranslation = new ItemTranslation();
            itemTranslation.setItem(item);
            itemTranslation.setContent("");
            itemTranslation.setDescription("");
            itemTranslation.setTitle("");
            itemTranslation.setItemTranslationPK(new ItemTranslation.ItemTranslationPK(id, 1L));
            itemTransRepo.save(itemTranslation);
        }
        ItemTranslation itemTranslation = new ItemTranslation();
        itemTranslation.setItem(item);
        itemTranslation.setContent(content);
        itemTranslation.setDescription(description);
        itemTranslation.setTitle(title);
        itemTranslation.setItemTranslationPK(new ItemTranslation.ItemTranslationPK(id, ConvertUtils.toLong(language_id)));
        itemTransRepo.save(itemTranslation);
        response.sendRedirect("/add_item");
    }


    @RequestMapping("/removeItem")
    public void removeItem(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") String id)
            throws IOException {
        itemDB.deleteById(Long.valueOf(id));
        response.sendRedirect("/items");
    }

    @RequestMapping("/item-content-detail")
    public String contentDetail(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        List<ItemTranslation> mItems = new ArrayList<>();
        ItemTranslation translation = itemTransRepo.findById(new ItemTranslation.ItemTranslationPK(id, 1L)).get();
        mItems.add(translation);
        model.addAttribute("listItems", mItems);
        return "content-detail";
    }

    @RequestMapping("/updateItem")
    public String updateItem(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        model.addAttribute("page_content", "update_item.ftl");
        List<MItem> mItemnew = new ArrayList();
        for (int i = 0; i < mItems.size(); i++) {
            if (mItems.get(i).getId() == id) {
                MItem mItem = mItems.get(i);
                mItemnew.add(mItem);
            }
        }
        List<MItem> mItemnewEn = new ArrayList();
        for (int i = 0; i < mItemsEn.size(); i++) {
            if (mItemsEn.get(i).getId() == id) {
                MItem mItem = mItemsEn.get(i);
                mItemnewEn.add(mItem);
            }
        }
        List<Project> projectList = projectDB.findAll();
        List<MProject> newList = new ArrayList();
        for (Project project : projectList) {
            ProjectTranslation projectTranslation = projectTransRepo.findByProjectIdAndLanguageCode(project.getId(), "vi");
            if (projectTranslation != null) {
                newList.add(updateProjectToModel(project, projectTranslation));
            }
        }
        model.addAttribute("projects", newList);
        model.addAttribute("listItems", mItemnew);
        if (mItemnewEn.size() != 0) {
            model.addAttribute("listItemsEn", mItemnewEn);
        } else {
            model.addAttribute("listItemsEn", mItemnew);
        }
        model.addAttribute("price", PRICE);
        model.addAttribute("menu", menu);
        model.addAttribute("title", title);
        model.addAttribute("description", description);
        model.addAttribute("content", content);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateItem")
    public void updateItem(@RequestParam("imgUrl") MultipartFile files, @RequestParam(name = "id", defaultValue = "0") Long id,
                           HttpServletRequest request, HttpServletResponse response) throws IOException {
        String content = request.getParameter("content");
        String title = request.getParameter("title");
        String description = request.getParameter("description");
        String project_id = request.getParameter("project_id");
        String price = request.getParameter("price");
        String language_id = request.getParameter("language_id");
        Project project = projectDB.findById(ConvertUtils.toLong(project_id)).get();
        Item item = itemDB.getOne(id);
        item.setProject(project);
        if (PRICE == true) {
            if (!price.isEmpty()) {
                item.setPrice(ConvertUtils.toDouble(price));
            }
        }
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            item.setImgUrl(UPLOAD_FOLDER + files.getOriginalFilename());
        }
        item.setProject(project);
        itemDB.save(item);
        ItemTranslation itemTranslation = new ItemTranslation();
        itemTranslation.setItem(item);
        itemTranslation.setTitle(title);
        itemTranslation.setDescription(description);
        itemTranslation.setContent(content);
        itemTranslation.setItemTranslationPK(new ItemTranslation.ItemTranslationPK(id, ConvertUtils.toLong(language_id)));
        itemTransRepo.save(itemTranslation);
        response.sendRedirect("/items");
    }

}
