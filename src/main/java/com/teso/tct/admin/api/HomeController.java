package com.teso.tct.admin.api;

import com.teso.tct.admin.cache.AdminInfoCA;
import com.teso.tct.admin.db.AdminInfoDB;
import com.teso.tct.admin.db.CustomerInfoDB;
import com.teso.tct.admin.db.PermissionInfoDB;
import com.teso.tct.admin.ent.AdminInfo;
import com.teso.tct.admin.ent.CustomerInfo;
import com.teso.tct.admin.model.MAdminInfo;
import com.teso.tct.admin.model.MCustomerInfo;
import com.teso.tct.admin.model.MPermissionInfo;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

  @Autowired
  private CustomerInfoDB customerInfoDB;
  @Autowired
  private AdminInfoDB adminInfoDB;

  @Autowired
  private PermissionInfoDB permissionInfoDB;

  @RequestMapping("/home")
  public String home(Model model, HttpServletRequest request, HttpServletResponse response) {
    model.addAttribute("active_admin_info", true);
    List<MAdminInfo> mAdminInfoList = adminInfoDB.findAll().stream().map(p
            -> new MAdminInfo(p.getId(), p.getName(), p.getEmail(), p.getRole(), p.getPermissionInfo().getName()))
            .collect(Collectors.toList());
    model.addAttribute("page_content", "admin.ftl");
    model.addAttribute("mAdminInfoList", mAdminInfoList);
    List<MPermissionInfo> mPermissionInfoList = permissionInfoDB.findAll().stream().map(p -> {
      boolean canRemove = CollectionUtils.isEmpty(p.getAdminInfoList());
      return new MPermissionInfo(p.getId(), p.getName(), p.getRule(), canRemove);
    }).collect(Collectors.toList());
    model.addAttribute("mPermissionInfoList", mPermissionInfoList);
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return ApiAuthenUtils.getAdminInfo(authentication, model, "index");  }

  @RequestMapping("/")
  public void index(Model model, HttpServletRequest request, HttpServletResponse response)
      throws IOException {
    response.sendRedirect("/home");
  }
}
