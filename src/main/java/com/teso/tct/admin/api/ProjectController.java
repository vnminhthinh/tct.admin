package com.teso.tct.admin.api;

import com.teso.framework.utils.ConvertUtils;
import com.teso.framework.utils.JSONUtil;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.MenuRepo;
import com.teso.tct.admin.db.MenuTransRepo;
import com.teso.tct.admin.db.ProjectDB;
import com.teso.tct.admin.db.ProjectTransRepo;
import com.teso.tct.admin.ent.Menu;
import com.teso.tct.admin.ent.MenuTranslation;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.ent.ProjectTranslation;
import com.teso.tct.admin.model.MMenu;
import com.teso.tct.admin.model.MProject;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static com.teso.tct.admin.config.EntitiyExtension.updateMenuToModel;
import static com.teso.tct.admin.config.EntitiyExtension.updateProjectToModel;

@Controller
public class ProjectController {
    @Autowired
    private ProjectDB projectDB;
    @Autowired
    PageService pageService;
    @Autowired
    private MenuRepo menuRepo;
    @Autowired
    private MenuTransRepo menuTransRepo;
    @Autowired
    private ProjectTransRepo projectTransRepo;
    @Autowired
    PageServiceProject pageServiceProject;

    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private int PAGE_SIZE = ConvertUtils.toInt(ConfigInfo.PAGE_SIZE);
    private boolean description = ConvertUtils.toBoolean(ConfigInfo.DESCRIPTION);
    private boolean content_project = ConvertUtils.toBoolean(ConfigInfo.CONTENT_PROJECT);
    private String IMAGE_DEFAULT = ConfigInfo.IMAGE_DEFAULT;
    private boolean type = ConvertUtils.toBoolean(ConfigInfo.TYPE);
    private boolean menu = ConvertUtils.toBoolean(ConfigInfo.MENU);
    private boolean THUMBNAIL=ConvertUtils.toBoolean(ConfigInfo.THUMBNAIL_PROJECT);
    private List<MProject> projectModels;
    private List<MProject> projectModelsEn;

    public ProjectController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/projects")
    public String projects(Model model, @RequestParam(name = "page", defaultValue = "1") int page) {
        model.addAttribute("page_content", "project.ftl");
        model.addAttribute("upload_url", UPLOAD_URL);
        List<Project> projects = projectDB.findAll();
        int totalPage = projects.size();
        int j = totalPage % PAGE_SIZE;
        int k = totalPage / PAGE_SIZE;
        if (j > 0) {
            k++;
        }
        projectModels = new ArrayList();
        projectModelsEn = new ArrayList();
        for (int i = (page - 1) * PAGE_SIZE; i < page * PAGE_SIZE && i < projects.size(); i++) {
            ProjectTranslation projectTranslation = projectTransRepo.findByProjectIdAndLanguageCode(projects.get(i).getId(), "vi");
            Menu menu = projects.get(i).getMenu();
            if (menu != null) {
                if (projectTranslation != null) {
                    MenuTranslation menuTranslation = menuTransRepo.findById(new MenuTranslation.MenuTranslationPK(menu.getId(), 1L)).get();
                    MProject projectModel = updateProjectToModel(projects.get(i), projectTranslation, menu, menuTranslation);
                    projectModels.add(projectModel);
                }
            } else {
                if (projectTranslation != null) {
                    MProject projectModel = updateProjectToModel(projects.get(i), projectTranslation);
                    projectModels.add(projectModel);
                }
            }
            ProjectTranslation projectTranslationEn = projectTransRepo.findByProjectIdAndLanguageCode(projects.get(i).getId(), "en");
            Menu menuEn = projects.get(i).getMenu();
            if (menuEn != null) {
                if (projectTranslationEn != null) {
                    MenuTranslation menuTranslation = menuTransRepo.findById(new MenuTranslation.MenuTranslationPK(menuEn.getId(), 1L)).get();
                    MProject projectModel = updateProjectToModel(projects.get(i), projectTranslationEn, menuEn, menuTranslation);
                    projectModelsEn.add(projectModel);
                }
            } else {
                if (projectTranslationEn != null) {
                    MProject projectModel = updateProjectToModel(projects.get(i), projectTranslationEn);
                    projectModelsEn.add(projectModel);
                }
            }
        }
        model.addAttribute("content", content_project);
        model.addAttribute("projectList", projectModels);
        model.addAttribute("page", k);
        model.addAttribute("nextPage", page + 1);
        model.addAttribute("prePage", page - 1);
        model.addAttribute("currentPage", page);
        model.addAttribute("menu", menu);
        model.addAttribute("description", description);
        model.addAttribute("type", type);
        model.addAttribute("thumbnail",THUMBNAIL);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_project")
    public String addProject(Model model, HttpServletRequest request, HttpServletResponse response) {
        model.addAttribute("page_content", "add_project.ftl");

        List<Menu> listMenus = menuRepo.findAll();
        List<MMenu> newListMenus = new ArrayList();
        for (Menu menu : listMenus) {
            MenuTranslation menuTranslation = menuTransRepo.findByMenuIdAndLanguageCode(menu.getId(), "vi");
            if (menuTranslation != null) {
                newListMenus.add(updateMenuToModel(menu, menuTranslation));
            }
        }
        model.addAttribute("menuList", newListMenus);
        model.addAttribute("menu", menu);
        model.addAttribute("description", description);
        model.addAttribute("content", content_project);
        model.addAttribute("type", type);
        model.addAttribute("thumbnail",THUMBNAIL);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("createProjectThumbnail")
    public void createProject(@RequestParam("thumbnail") MultipartFile files, HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        String content = request.getParameter("content");
        String language_id = request.getParameter("language_id");
        Project project = new Project();
        if (menu) {
            Menu menu = menuRepo.findById(ConvertUtils.toLong(menu_id)).get();
            project.setMenu(menu);
        }
        project.setType(type);
        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            project.setThumbnail(UPLOAD_FOLDER + files.getOriginalFilename());
        } else {
            project.setThumbnail(UPLOAD_FOLDER + IMAGE_DEFAULT);
        }

        long id = projectDB.save(project).getId();
        if(ConvertUtils.toLong(language_id)==2){
            ProjectTranslation projectTranslation = new ProjectTranslation();
            projectTranslation.setName("");
            projectTranslation.setContent("");
            projectTranslation.setDescription("");
            ProjectTranslation.ProjectTranslationPK projectTranslationPK = new ProjectTranslation.ProjectTranslationPK();
            projectTranslationPK.setLanguageId(1L);
            projectTranslationPK.setProjectNonStranId(id);
            projectTranslation.setProjectTranslationPK(projectTranslationPK);
            projectTransRepo.save(projectTranslation);
        }
        ProjectTranslation projectTranslation = new ProjectTranslation();
        projectTranslation.setName(name);
        projectTranslation.setContent(content);
        projectTranslation.setDescription(description);
        ProjectTranslation.ProjectTranslationPK projectTranslationPK = new ProjectTranslation.ProjectTranslationPK();
        projectTranslationPK.setLanguageId(ConvertUtils.toLong(language_id));
        projectTranslationPK.setProjectNonStranId(id);
        projectTranslation.setProjectTranslationPK(projectTranslationPK);
        projectTransRepo.save(projectTranslation);
        response.sendRedirect("/add_project");
    }
    @PostMapping("createProject")
    public void createProject( HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        String content = request.getParameter("content");
        String language_id = request.getParameter("language_id");
        Project project = new Project();
        if (menu) {
            Menu menu = menuRepo.findById(ConvertUtils.toLong(menu_id)).get();
            project.setMenu(menu);
        }
        project.setType(type);
        long id = projectDB.save(project).getId();
        if(ConvertUtils.toLong(language_id)==2){
            ProjectTranslation projectTranslation = new ProjectTranslation();
            projectTranslation.setName("");
            projectTranslation.setContent("");
            projectTranslation.setDescription("");
            ProjectTranslation.ProjectTranslationPK projectTranslationPK = new ProjectTranslation.ProjectTranslationPK();
            projectTranslationPK.setLanguageId(1L);
            projectTranslationPK.setProjectNonStranId(id);
            projectTranslation.setProjectTranslationPK(projectTranslationPK);
            projectTransRepo.save(projectTranslation);
        }
        ProjectTranslation projectTranslation = new ProjectTranslation();
        projectTranslation.setName(name);
        projectTranslation.setContent(content);
        projectTranslation.setDescription(description);
        ProjectTranslation.ProjectTranslationPK projectTranslationPK = new ProjectTranslation.ProjectTranslationPK();
        projectTranslationPK.setLanguageId(ConvertUtils.toLong(language_id));
        projectTranslationPK.setProjectNonStranId(id);
        projectTranslation.setProjectTranslationPK(projectTranslationPK);
        projectTransRepo.save(projectTranslation);
        response.sendRedirect("/add_project");
    }

    @RequestMapping("/project-content-detail")
    public String contentDetail(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        List<ProjectTranslation> mProject = new ArrayList<>();
        ProjectTranslation translation = projectTransRepo.findById(new ProjectTranslation.ProjectTranslationPK(id, 1L)).get();
        mProject.add(translation);
        model.addAttribute("listItems", mProject);
        return "content-detail";
    }

    @PostMapping("createProjectMenu")
    public void createProjectMenu(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");

        Menu menu = menuRepo.findById(ConvertUtils.toLong(menu_id)).get();
        Project project = new Project();
//        project.setId(id);
//        project.setName(name);
//        project.setDescription(description);
        project.setType(type);
        if (Boolean.parseBoolean(ConfigInfo.MENU)) {
            project.setMenu(menu);
        }
        projectDB.save(project);
        response.sendRedirect("/add_project");
    }

    @RequestMapping("/removeProject")
    public void removeProject(HttpServletRequest request, HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") Long id)
            throws IOException {
        projectDB.deleteById(id);
        response.sendRedirect("/projects");
    }

    @RequestMapping("/updateProject")
    public String updateProject(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        model.addAttribute("page_content", "update_project.ftl");
        List<MProject> projects = new ArrayList();
        for (int i = 0; i < projectModels.size(); i++) {
            if (projectModels.get(i).getId() == id) {
                MProject mProject = projectModels.get(i);
                projects.add(mProject);
            }

        }
        List<Menu> listMenus = menuRepo.findAll();
        List<MMenu> newListMenus = new ArrayList();
        for (Menu menu : listMenus) {
            MenuTranslation menuTranslation = menuTransRepo.findByMenuIdAndLanguageCode(menu.getId(), "vi");
            if (menuTranslation != null) {
                newListMenus.add(updateMenuToModel(menu, menuTranslation));
            }

        }
        List<MProject> projectsen = new ArrayList();
        for (int i = 0; i < projectModelsEn.size(); i++) {
            if (projectModelsEn.get(i).getId() == id) {
                MProject mProject = projectModelsEn.get(i);
                projectsen.add(mProject);
            }

        }
        model.addAttribute("content", content_project);
        model.addAttribute("projects", projects);
        if (projectsen.size() != 0) {
            model.addAttribute("projectsEn", projectsen);
        } else {
            model.addAttribute("projectsEn", projects);
        }
        model.addAttribute("menuList", newListMenus);
        model.addAttribute("description", description);
        model.addAttribute("type", type);
        model.addAttribute("thumbnail",THUMBNAIL);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateProjectThumbnail")
    public void updateProject(@RequestParam("thumbnail") MultipartFile files, @RequestParam(name = "id", defaultValue = "0") Long id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        String language_id = request.getParameter("language_id");
        String content = request.getParameter("content");
        Project project = projectDB.getOne(id);
        if (menu) {
            Menu menu = menuRepo.findById(ConvertUtils.toLong(menu_id)).get();
            project.setMenu(menu);
        }
        project.setType(type);

        if (!files.getOriginalFilename().isEmpty()) {
            storageService.store(files);
            project.setThumbnail(UPLOAD_FOLDER + files.getOriginalFilename());
        }
        projectDB.save(project);
        ProjectTranslation projectTranslation = new ProjectTranslation();
        projectTranslation.setName(name);
        projectTranslation.setDescription(description);
        ProjectTranslation.ProjectTranslationPK projectTranslationPK = new ProjectTranslation.ProjectTranslationPK();
        projectTranslationPK.setLanguageId(ConvertUtils.toLong(language_id));
        projectTranslationPK.setProjectNonStranId(id);
        projectTranslation.setProjectTranslationPK(projectTranslationPK);
        if(content_project){
            projectTranslation.setContent(content);
        }else {
            projectTranslation.setContent("");
        }

        projectTransRepo.save(projectTranslation);
        response.sendRedirect("/projects");
    }
    @PostMapping("/updateProject")
    public void updateProject( @RequestParam(name = "id", defaultValue = "0") Long id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String menu_id = request.getParameter("menu_id");
        String language_id = request.getParameter("language_id");
        String content = request.getParameter("content");
        Project project = projectDB.getOne(id);
        if (menu) {
            Menu menu = menuRepo.findById(ConvertUtils.toLong(menu_id)).get();
            project.setMenu(menu);
        }
        project.setType(type);
        projectDB.save(project);
        ProjectTranslation projectTranslation = new ProjectTranslation();
        projectTranslation.setName(name);
        projectTranslation.setDescription(description);
        ProjectTranslation.ProjectTranslationPK projectTranslationPK = new ProjectTranslation.ProjectTranslationPK();
        projectTranslationPK.setLanguageId(ConvertUtils.toLong(language_id));
        projectTranslationPK.setProjectNonStranId(id);
        projectTranslation.setProjectTranslationPK(projectTranslationPK);
        if(content_project){
            projectTranslation.setContent(content);
        }else {
            projectTranslation.setContent("");
        }

        projectTransRepo.save(projectTranslation);
        response.sendRedirect("/projects");
    }

//    @PostMapping("/updateProjectMenu")
//    public void updateProject(@RequestParam(name = "id", defaultValue = "0") Long id,
//                              HttpServletRequest request, HttpServletResponse response) throws IOException {
//        String name = request.getParameter("name");
//        String description = request.getParameter("description");
//        String type = request.getParameter("type");
//        String menu_id = request.getParameter("menu_id");
//        Menu menu = menuRepo.findById(ConvertUtils.toLong(menu_id)).get();
//        Project project = projectDB.getOne(id);
////        if (!description.isEmpty()) {
////            project.setDescription(description);
////        } else {
////            project.setDescription("");
////        }
////        if (!name.isEmpty()) {
////            project.setName(name);
////        } else {
////            project.setName("");
////        }
//        if (!type.isEmpty()) {
//            project.setType(type);
//        } else {
//            project.setType("");
//        }
//        project.setMenu(menu);
//        projectDB.save(project);
//        response.sendRedirect("/projects");
//    }
}
