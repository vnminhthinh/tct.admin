package com.teso.tct.admin.api;

import com.teso.framework.utils.ConvertUtils;
import com.teso.tct.admin.config.ConfigInfo;
import com.teso.tct.admin.db.LanguageRepo;
import com.teso.tct.admin.db.MenuRepo;
import com.teso.tct.admin.db.MenuTransRepo;
import com.teso.tct.admin.ent.Language;
import com.teso.tct.admin.ent.Menu;
import com.teso.tct.admin.ent.MenuTranslation;
import com.teso.tct.admin.ent.Project;
import com.teso.tct.admin.model.MMenu;
import com.teso.tct.admin.service.PageService;
import com.teso.tct.admin.service.PageServiceProject;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.utils.ApiAuthenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

import static com.teso.tct.admin.config.EntitiyExtension.updateMenuToModel;

@Controller
public class MenuController {
    @Autowired
    private MenuRepo menuRepo;
    @Autowired
    PageService pageService;
    @Autowired
    private LanguageRepo languageRepo;
    @Autowired
    private MenuTransRepo menuTransRepo;
    @Autowired
    PageServiceProject pageServiceProject;
    private final StorageService storageService;
    private final String UPLOAD_URL = ConfigInfo.UPLOAD_URL;
    private final String UPLOAD_FOLDER = ConfigInfo.UPLOAD_FOLDER;
    private int PAGE_SIZE = Integer.parseInt(ConfigInfo.PAGE_SIZE);
    private MMenu mMenu;

    public MenuController(StorageService storageService) {
        this.storageService = storageService;
    }

    @RequestMapping("/menus")
    private String menu(Model model) {
        model.addAttribute("page_content", "menus.ftl");
        model.addAttribute("upload_url", UPLOAD_URL);
        List<Menu> listMenus = menuRepo.findAll();
        List<MMenu> newListMenus = new ArrayList();
        for (Menu menu : listMenus) {
            MenuTranslation menuTranslation = menuTransRepo.findByMenuIdAndLanguageCode(menu.getId(), "vi");
            if (menuTranslation != null) {
                newListMenus.add(updateMenuToModel(menu, menuTranslation));
            }
        }
        model.addAttribute("menuList", newListMenus);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @RequestMapping("/add_menu")
    public String addProject(Model model) {
        model.addAttribute("page_content", "add_menu.ftl");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("createMenu")
    public void createMenu(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String language_id = request.getParameter("language_id");
        Menu menu = new Menu();
        if (type != null) {
            menu.setType(type);
        } else {
            menu.setType("");
        }
        Long id = menuRepo.save(menu).getId();
        if(ConvertUtils.toLong(language_id)==2) {
            MenuTranslation menuTranslation = new MenuTranslation();
            menuTranslation.setDescription("");
            menuTranslation.setName("");
            MenuTranslation.MenuTranslationPK translationPK = new MenuTranslation.MenuTranslationPK();
            translationPK.setLanguageId(1L);
            translationPK.setMenuNonStranId(id);
            menuTranslation.setMenuTranslationPK(translationPK);
            menuTransRepo.save(menuTranslation);
        }
        MenuTranslation menuTranslation = new MenuTranslation();
        menuTranslation.setDescription(description);
        menuTranslation.setName(name);
        MenuTranslation.MenuTranslationPK translationPK = new MenuTranslation.MenuTranslationPK();
        translationPK.setLanguageId(ConvertUtils.toLong(language_id));
        translationPK.setMenuNonStranId(id);
        menuTranslation.setMenuTranslationPK(translationPK);
        menuTransRepo.save(menuTranslation);
        response.sendRedirect("/add_menu");
    }

    @RequestMapping("/removeMenu")
    public void removeProject(HttpServletResponse response, @RequestParam(name = "id", defaultValue = "0") Long id)
            throws IOException {
        menuRepo.deleteById(id);
        response.sendRedirect("/menus");
    }

    @RequestMapping("/updateMenu")
    public String updateProject(Model model, @RequestParam(name = "id", defaultValue = "0") Long id) {
        model.addAttribute("page_content", "update_menu.ftl");
        List<MMenu> mMenuList = new ArrayList<>();
        Menu menu = menuRepo.findById(id).get();
        MenuTranslation menuTranslation = menuTransRepo.findByMenuIdAndLanguageCode(menu.getId(), "vi");
        MMenu menuModel = updateMenuToModel(menu, menuTranslation);
        mMenuList.add(menuModel);
        model.addAttribute("menuList", mMenuList);
        List<MMenu> mMenuListEn = new ArrayList<>();
        Menu menuEn = menuRepo.findById(id).get();
        MenuTranslation menuTranslationEn = menuTransRepo.findByMenuIdAndLanguageCode(menu.getId(), "en");
        if (menuTranslationEn != null) {
            MMenu menuModelEn = updateMenuToModel(menuEn, menuTranslationEn);
            mMenuListEn.add(menuModelEn);
        }
        if (mMenuListEn.size() != 0) {
            model.addAttribute("menuListEn", mMenuListEn);
        } else {
            model.addAttribute("menuListEn", mMenuList);
        }
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return ApiAuthenUtils.getAdminInfo(authentication, model, "index");
    }

    @PostMapping("/updateMenu")
    public void updateProject(@RequestParam(name = "id", defaultValue = "0") Long id,
                              HttpServletRequest request, HttpServletResponse response) throws IOException {
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        String type = request.getParameter("type");
        String language_id = request.getParameter("language_id");
        Menu menu = new Menu();
        menu.setId(id);
        menu.setCreatedTime(new Date(System.currentTimeMillis()));
        if (type != null) {
            menu.setType(type);
        } else {
            menu.setType("");
        }
        menuRepo.save(menu);
        MenuTranslation menuTranslation = new MenuTranslation();
        menuTranslation.setDescription(description);
        menuTranslation.setName(name);
        MenuTranslation.MenuTranslationPK translationPK = new MenuTranslation.MenuTranslationPK();
        translationPK.setLanguageId(ConvertUtils.toLong(language_id));
        translationPK.setMenuNonStranId(id);
        menuTranslation.setMenuTranslationPK(translationPK);
        menuTransRepo.save(menuTranslation);
        response.sendRedirect("/menus");
    }
}
