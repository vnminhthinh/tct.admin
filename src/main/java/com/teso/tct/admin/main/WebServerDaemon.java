package com.teso.tct.admin.main;

import com.teso.tct.admin.config.ConfigInfo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import com.teso.tct.admin.storage.StorageService;
import com.teso.tct.admin.storage.StorageProperties;
@SpringBootApplication(scanBasePackages = {"com.teso"})
@EnableJpaAuditing
@EntityScan("com.teso")
@EnableJpaRepositories("com.teso")
@EnableConfigurationProperties(StorageProperties.class)

public class WebServerDaemon {

    private static final String TAG = WebServerDaemon.class.getName();

    public static void main(String[] args) {
        System.err.println("==============>" + ConfigInfo.SPRING_BOOT_CONFIG);
        ConfigurableApplicationContext applicationContext = new SpringApplicationBuilder(
            WebServerDaemon.class)
                .properties("spring.config.location:" + ConfigInfo.SPRING_BOOT_CONFIG)
                .build().run(args);
    }

}
