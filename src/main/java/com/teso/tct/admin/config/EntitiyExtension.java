package com.teso.tct.admin.config;

import com.teso.tct.admin.db.MenuTransRepo;
import com.teso.tct.admin.ent.*;
import com.teso.tct.admin.model.MItem;
import com.teso.tct.admin.model.MMenu;
import com.teso.tct.admin.model.MProject;
import org.springframework.beans.factory.annotation.Autowired;


public class EntitiyExtension {

    public static MMenu updateMenuToModel(Menu menu, MenuTranslation menuTranslation) {
        MMenu menuModel = new MMenu();
        menuModel.setId(menu.getId());
        menuModel.setName(menuTranslation.getName());
        menuModel.setDescription(menuTranslation.getDescription());
        menuModel.setContent(menuTranslation.getContent());
        menuModel.setThumbnail(menu.getThumbnail());
        menuModel.setType(menu.getType());
        menuModel.setCreatedTime(menu.getCreatedTime());
        return menuModel;
    }

    public static MProject updateProjectToModel(Project project, ProjectTranslation projectTranslation) {
        MProject projectModel = new MProject();
        projectModel.setId(project.getId());
        projectModel.setName(projectTranslation.getName());
        projectModel.setDescription(projectTranslation.getDescription());
        projectModel.setContent(projectTranslation.getContent());
        projectModel.setThumbnail(project.getThumbnail());
        projectModel.setType(project.getType());
        projectModel.setCreatedTime(project.getCreatedTime());
        return projectModel;
    }

    public static MProject updateProjectToModel(Project project, ProjectTranslation projectTranslation, Menu menu, MenuTranslation menuTranslation) {
        MProject projectModel = new MProject();
        projectModel.setId(project.getId());
        projectModel.setName(projectTranslation.getName());
        projectModel.setDescription(projectTranslation.getDescription());
        projectModel.setContent(projectTranslation.getContent());
        projectModel.setThumbnail(project.getThumbnail());
        projectModel.setType(project.getType());
        projectModel.setCreatedTime(project.getCreatedTime());
        if (menu != null) {
            projectModel.setmMenu(updateMenuToModel(menu, menuTranslation));
        }
        return projectModel;
    }

    public static MItem updateItemToModel(Item item, ItemTranslation itemTranslation) {
        MItem itemModel = new MItem();
        itemModel.setContent(itemTranslation.getContent());
        itemModel.setCreatedTime(item.getCreatedTime());
        itemModel.setDescription(itemTranslation.getDescription());
        itemModel.setId(item.getId());
        itemModel.setImgUrl(item.getImgUrl());
        itemModel.setPrice(item.getPrice());
        itemModel.setTitle(itemTranslation.getTitle());
        return itemModel;
    }

    public static MItem updateItemToModel(Item item, ItemTranslation itemTranslation, Project project, ProjectTranslation projectTranslation) {
        MItem itemModel = new MItem();
        itemModel.setContent(itemTranslation.getContent());
        itemModel.setCreatedTime(item.getCreatedTime());
        itemModel.setDescription(itemTranslation.getDescription());
        itemModel.setId(item.getId());
        itemModel.setImgUrl(item.getImgUrl());
        itemModel.setPrice(item.getPrice());
        itemModel.setPromotion(item.getPromotion());
        itemModel.setTitle(itemTranslation.getTitle());
        itemModel.setmProject(updateProjectToModel(project, projectTranslation));
        return itemModel;
    }


}
